class Person {
    constructor(firstName, lastName, accounts){
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    }
    _calculateBalance(){
        var sum = 0;
        for(var account of this.accounts){
            sum += account.balance;
        }
        return sum;
    }
    sayHallo() {
        return "Name: " + this.firstName + ", Surname: " + this.lastName + ", Accounts: " + this.accounts.length + ", Total balance: " + this._calculateBalance() ;
    }
    addAccount(account) {
        this.accounts.push(account);
    }
    filterPositiveAccounts(){
        return this.accounts.filter(account => account.balance > 0);
    }
    findAccount(accountNumber){
        return this.accounts.find(function(account){return account.number === accountNumber});
    }
    withdraw(accountNumber, amount){
        return new Promise((resolve, reject) => {
            let thisPersonsAccount = this.findAccount(accountNumber);
            if(thisPersonsAccount != undefined && amount <= (thisPersonsAccount.balance)){
                //console.log("before resolution")
                thisPersonsAccount.balance -= amount;
                setTimeout(() => (resolve(`Account ${accountNumber} new balance: ${thisPersonsAccount.balance}, after withdrawal of ${amount}`)),3000);
            } else {
                reject("The account number does not exist or the amount to withdraw is too big");
            }
        })
    }
};

class Account{
    constructor(balance, currency, number){
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}

account1 = new Account (5000, "$", 1);
account2 = new Account (10000, "$", 2);
account3 = new Account (-1000, "$", 3);
account4 = new Account (7000, "$",4);

const person1 = new Person("Dominik", "Jakubowski", [account1, account2]);

person1.addAccount(account3);
person1.addAccount(account4);


// person1.withdraw(1,1000)
//     .then(function(successMessage){
//     console.log(successMessage);
//     })  
//     .catch(function(failMessage){
//     console.log(failMessage)
//     });
// person1.withdraw(4,20000)
//     .then(function(successMessage){
//     console.log(successMessage);
//     })
//     .catch(function(failMessage){
//     console.log(failMessage);
//     });
// person1.withdraw(8,20000)
//     .then(function(successMessage){
//     console.log(successMessage);
//     })
//     .catch(function(failMessage){
//     console.log(failMessage);
//     });


account5 = new Account(3000, "$", 5);
account6 = new Account(1500, "$", 6);

let person2 = new Person("John", "Smith",[account5, account6]);


display = (() => {

    function showPerson(){
    
        document.getElementById("person").innerHTML = person2.firstName + " " + person2.lastName;
        document.getElementById("account-select").appendChild(document.createElement("option"));
    
        for(account of person2.accounts){
            var newLine = document.createElement("p");
            var text = document.createTextNode("Account " + account.number + " -> " + account.currency + account.balance);
            newLine.appendChild(text);
            document.getElementById("account").appendChild(newLine);

            let newOption = document.createElement("option");
            newOption.value = account.number;
            newOption.text = account.number;
            document.getElementById("account-select").appendChild(newOption);
        }
    }

    window.onload = showPerson;

    function clearAndRefresh(){
    document.getElementById("account").innerHTML ="";
    document.getElementById("account-select").innerHTML="";
    document.getElementById("amount").value="";
    display.buttonToggle();
    showPerson();
    }



    return {

        buttonToggle :
        function buttonToggle(){
            let amountBox = document.getElementById("amount").value;
            let accountBox = document.getElementById("account-select").value;    //#number for the older version
            let submitButton = document.getElementById("submit-button");
            
            submitButton.disabled = !(amountBox && accountBox);
            
        }
        ,withdrawMoney :
        function withdrawMoney(){
            let accountNumber = parseInt(document.getElementById("account-select").value);    //#number for the older version
            let withdrawAmount = parseInt(document.getElementById("amount").value);
            console.log(person2);
            console.log(accountNumber);
            console.log(withdrawAmount);
        
            if(withdrawAmount != NaN && isFinite(withdrawAmount) && withdrawAmount >= 0){

                person2.withdraw(accountNumber, withdrawAmount)
                .then(function(successMessage){
                console.log(successMessage);
                clearAndRefresh();
                })
                .catch(function(failMessage){
                console.log(failMessage);
                alert(failMessage);
                });
            } else {
                alert("Please provide correct amount");
            }
        }
    }

})();
